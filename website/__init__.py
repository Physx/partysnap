from flask import Flask, render_template, request, jsonify
from pymongo import MongoClient
import qrcode
from io import BytesIO
from PIL import Image
import base64

import os

app = Flask(__name__)

# MongoDB-Verbindung
mongo_username = os.environ.get("MONGO_INITDB_ROOT_USERNAME")
mongo_password = os.environ.get("MONGO_INITDB_ROOT_PASSWORD")
mongo_host = "localhost"
mongo_port = 27017

# Verbindung zur MongoDB-Datenbank herstellen
client = MongoClient(
    f"mongodb://{mongo_username}:{mongo_password}@{mongo_host}:{mongo_port}/"
)
db = client["party_app"]
images_collection = db["images"]


# Indexseite (Startseite)
@app.route("/")
def index():
    return render_template("index.html")


# Endpunkt zum Abrufen der letzten 20 Bilder
@app.route("/get_latest_images", methods=["GET"])
def get_latest_images():
    # Die letzten 20 Bilder aus der MongoDB abrufen
    latest_images = images_collection.find().sort([("_id", -1)]).limit(8)
    image_urls = []
    for image in latest_images:
        # Bild in Base64 codieren und zur Liste hinzufügen
        image_base64 = base64.b64encode(image.get("image_data")).decode("utf-8")
        image_urls.append(image_base64)
    return jsonify({"image_urls": image_urls})


# QR-Code-Generator
@app.route("/generate_qr")
def generate_qr():
    # Erstellen Sie eine eindeutige ID für den Endpunkt
    endpoint_url = "https://192.168.178.22/take_picture"  # Der Endpunkt, auf den der QR-Code verweisen soll

    # Generieren Sie den QR-Code mit der URL des Endpunkts
    qr = qrcode.QRCode(
        version=1,
        error_correction=qrcode.constants.ERROR_CORRECT_L,
        box_size=10,
        border=4,
    )
    qr.add_data(endpoint_url)
    qr.make(fit=True)

    # Speichern Sie den QR-Code in einer temporären Datei
    qr_image_path = "website/static/qrcode.png"
    qr.make_image().save(qr_image_path)

    # Geben Sie den Pfad zum QR-Code-Bild zurück
    return jsonify({"qr_code": "/static/qrcode.png"})


# Endpunkt zum Anzeigen der Seite zum Aufnehmen eines Bildes
@app.route("/take_picture")
def take_picture():
    return render_template("take_picture.html")


# Bildspeicherung
@app.route("/save_image", methods=["POST"])
def save_image():
    image_data_base64 = request.form.get("image_data")
    image_data_binary = base64.b64decode(image_data_base64)

    # Hier sollte das Bild in der MongoDB gespeichert werden
    # Beispiel:
    images_collection.insert_one({"image_data": image_data_binary})

    return jsonify({"message": "Image saved successfully!"})


if __name__ == "__main__":
    context = (
        "/home/xxx/private/Zertifikate/2023/Romany_2023_cert.pem",
        "/home/xxx/private/Zertifikate/2023/Romany_2023_key.pem",
    )
    # app.run(debug=False, host="0.0.0.0", port=443, ssl_context=context)
    app.run(ssl_context="adhoc", host="0.0.0.0", port=443)
